# Assignment submitter

## Brief
This app should have a simple interface to submit a coding/writting assignment. 

Either by direct text or by uploading the file.

This is planned to be a web app so anyone can access it across platforms.

## Description

- Assignment creator can create a assignment 
- Learners can go this website and select the assignment they want to answer 
- Upload/submit the assignment they have selected
- Once submitted, the same assignment files needs to be saved in a single location and it can be downloaded as a zip by the Assignment creator.
- Assignment creator can clear the files after completion so it won't take unnecessary space.

Resubmission can be allowed

All the files uploaded must be uploaded with a specific formatted name. not the names of the file they upload. 

This standard format in the name helps in retrieving the file and in re-editing.

## Roles
Assignment creator - admin
Learner - user


